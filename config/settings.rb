ApiSpecsApp.config =
    {
        test: {
            glink: {
                protocol: 'http',
                address: 'api.qa.groundlink.us',
                mime_format: 'json',
                authentication: {
                    client_id: 'deem_integration_app',
                    client_secret: 'j4y8sdl6atfg'
                },
                valid_credentials: {
                    erp_deem: 'railsninja',
                    erp_danet: 'limo123',
                    customer_123: 'password_for_customer',
                    affiliate_22: 'password_for_affiliate'
                }
            }
        }
    }
