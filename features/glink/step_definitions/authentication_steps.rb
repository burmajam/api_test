Given(/^I'm logged in as (.+):(.+)$/) do |role, username|
  authorize(role, username)
end
