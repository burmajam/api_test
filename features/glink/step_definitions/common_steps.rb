Then(/^response status is (\d+)$/) do |status|
  response.code.should == status.to_i
end

Then(/^response body has fields?: (.+) in (.+) node$/) do |field_names, root_node|
  target_node = response.parsed_response
  root_node.split('/').each { |node| target_node = target_node[node] } unless root_node == 'root'
  field_names.split(',').each { |attr| target_node[attr.strip].should_not be_nil }
end

Then(/^response body has fields?: (.+) in (.+) node with values?: (.+)$/) do |field_names, root_node, field_values|
  test_response_field_values(field_names, root_node, field_values) do |node, value|
    node.should == value
  end
end

Then(/^response body has fields?: (.+) in (.+) node containing values?: (.+)$/) do |field_names, root_node, field_values|
  test_response_field_values(field_names, root_node, field_values) do |node, value|
    node.should include(value)
  end
end

private

def test_response_field_values(field_names, root_node, field_values)
  target_node = response.parsed_response
  root_node.split('/').each { |node| target_node = target_node[node] } unless root_node == 'root'
  field_values = field_values.split(',')
  field_names.split(',').each_with_index do |attr, index|
    target_node[attr.strip].should_not be_nil
    yield target_node[attr.strip], field_values[index].strip
  end
end