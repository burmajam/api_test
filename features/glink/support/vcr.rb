unless defined? VCR
  require 'vcr'

  VCR.configure do |config|
    config.cassette_library_dir = 'cassettes'
    config.default_cassette_options = {record: :once}
    config.hook_into :webmock
    config.filter_sensitive_data('<GLINK_CLIENT_SECRET>') { ApiSpecsApp.config[:glink][:authentication][:client_secret] }
    ApiSpecsApp.config[:glink][:valid_credentials].each_value do |v|
      config.filter_sensitive_data('<GLINK_PASSWORD>') { v }
    end
    config.allow_http_connections_when_no_cassette = true
  end
end