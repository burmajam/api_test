# Test service: POST: /carservice/capacity/validate
# with body:
# {
#   "car_class": "#{car_class}",
#   "adults": #{pax_count},
#   "luggage": #{luggage_count},
#   "child_seats": #{baby_seats}
# }

Feature: Checking car capacity
  Background:
    Given I'm logged in as erp:danet

  # Assert that response code is 204 for LX car class with 1 passenger, 3 luggage and 2 child seats
  @check-car-capacity-valid
  Scenario: Check car capacity for valid combination

  # Assert that response code is 409 for LX car class with 5 passengers, 3 luggage and 2 child seats
  # and response body includes error message: Maximum number of passengers is 3
  @check-car-capacity-invalid
  Scenario: Check car capacity for invalid combination
