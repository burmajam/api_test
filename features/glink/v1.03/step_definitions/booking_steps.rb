When(/^I create new booking with valid cc$/) do
  search_id = limo_result_for_car_class "LX"
  limo_node = @limo_node.merge search_id: search_id
  booking_request = fixture_for 'BookingRequest'
  process_http_request :post, '/bookings', body: booking_request.simple_erp_like_build(limo: limo_node)
end
