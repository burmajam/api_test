require_relative '../../support/vcr'
VCR.configure { |config| config.cassette_library_dir = "cassettes/glink/#{ApiWorld.api_version}" }

VCR.cucumber_tags do |t|
  proxy_to_api = {record: :all}

  t.tag 'erp-create-valid-airport-pu-reservation', proxy_to_api
  t.tag 'check-car-capacity-valid', proxy_to_api
  t.tag 'check-car-capacity-invalid', proxy_to_api
end
