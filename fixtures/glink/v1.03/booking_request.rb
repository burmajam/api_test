module Glink
  module V1_03
    class BookingRequest
      def self.build(options={})
        <<JSON
{
    "booking": {
        "reservations": [{
            "limo": {
                "type": "POINT_TO_POINT",
                "adults": 1,
                "luggage": 0,
                "schedule": {
                    "type": "LATER",
                    "value": "05/25/2014 09:10 PM"
                },
                "pickup": {
                    "location": {
                        "name": "134 West 37th Street, New York, NY 10018, USA",
                        "address": "134 West 37th Street",
                        "city": "New York",
                        "postal_code": "10018",
                        "region": "NY",
                        "country": "US",
                        "latitude": 40.752344,
                        "longitude": -73.988527,
                        "category": "Address",
                        "partial_match": false
                    },
                    "user_address": "134, west 37th street, 10018, New York, NY, US"
                },
                "stops": [],
                "customer_comments": "",
                "destination": {
                    "location": {
                        "name": "29 West 12th Street, New York, NY 10011, USA",
                        "address": "29 West 12th Street",
                        "city": "New York",
                        "postal_code": "10011",
                        "region": "NY",
                        "country": "US",
                        "latitude": 40.735334,
                        "longitude": -73.995596,
                        "category": "Address",
                        "partial_match": false
                    },
                    "user_address": "29, W 12th St New York, 10011, New York, NY, US"
                },
                "vehicle_type": "LX",
                "search_id": #{options[:search_id]}
            }
        }],
        "passengers": [{
            "name": "Milan Burmaja",
            "email": "mburmaja170@groundlink.com",
            "phones": ["555-555-3111", "555-555-5555", "555-555-4111", "555-555-2111"],
            "id": 1143211
        }],
        "payment": {
            "type": "ACCOUNT",
            "credit_card": {
                "type": "VISA",
                "number": "4111111111111111",
                "expiration": "02/14",
                "holder_name": "Milan Burmaja",
                "cc_id": 432728
            },
            "account": {
                "number": "WG100084"
            },
            "id": "432728"
        }
    }
}
JSON
      end

      def self.simple_erp_like_build(options={})
        options = options[:limo] || {}
        options.merge! search_id: options[:search_id]
        <<JSON
{
    "booking": {
        "reservations": [{
            "limo":  #{LimoNodes.build(options).to_json}
        }],
        "passengers": [{
            "name": "Milan Burmaja",
            "email": "mburmaja170@groundlink.com",
            "phones": ["555-555-3111"]
        }],
        "payment": {
            "type": "CREDIT_CARD",
            "credit_card": {
                "type": "VISA",
                "number": "4111111111111111",
                "expiration": "02/14",
                "holder_name": "Milan Burmaja"
            }
        }
    }
}
JSON
      end
    end
  end
end
