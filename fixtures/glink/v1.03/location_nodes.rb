module LocationNodes
  def self.flight_details(options={})
    airline = options[:airline] || 'AA'
    flight_number = options[:flight_number] || 'flt 123'
    meeting_area = options[:meeting_area] || 'Out'
    track_flight = options[:track_flight] || true
    airport_pickup_offset = options[:airport_pickup_offset] || 0
    {
        "airline" => airline,
        "number" => flight_number,
        "arrival-from" => "BEG",
        "landing-time" => "",
        "meeting-area" => meeting_area,
        "track-flight" => track_flight,
        "airport_pickup_offset" => airport_pickup_offset
    }
  end

  def self.jfk
    {
        "latitude" => 40.6397511111,
        "longitude" => -73.7789255556,
        "name" => "JOHN F KENNEDY INTL, NEW YORK, NY, USA",
        "category" => "Airport",
        "iata" => "JFK",
        "address" => "",
        "postal_code" => "",
        "city" => "NEW YORK",
        "region" => "NY",
        "country" => "USA"
    }
  end

  def self.office
    {
        "latitude" => 40.752344,
        "longitude" => -73.988527,
        "name" => "134 West 37th Street, New York, NY 10018, USA",
        "category" => "Address",
        "address" => "134 West 37th Street",
        "postal_code" => "10018",
        "city" => "New York",
        "region" => "NY",
        "country" => "US"
    }
  end
end
