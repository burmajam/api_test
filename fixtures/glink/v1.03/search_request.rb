module Glink
  module V1_03
    class SearchRequest
      def self.build_request(options={})
        ::Glink::V1_02::SearchRequest.build_request options
      end

      def self.build_customized_request(options={})
        limo_node = options[:limo_node] || LimoNodes.build(options[:limo_options])
        result = <<JSON
{
    "limo": #{limo_node.to_json}
}
JSON
        result
      end
    end
  end
end
