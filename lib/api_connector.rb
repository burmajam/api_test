require 'httparty'

module ApiConnector
  class ApiUnavailable < StandardError; end

  class ApiError < StandardError
    attr_reader :code, :message, :body

    def initialize(message, code = nil, body=nil)
      @message, @code, @body = message, code, body
    end

    def to_s
      [@code, @message].join ' : '
    end
  end

  def self.included(base)
    base.instance_eval {
      include HTTParty
      include InstanceMethods
      #debug_output unless DeemIntegrator.env == :production
    }
    base.extend ClassMethods
  end

  module InstanceMethods
    def settings
      self.class.settings
    end

    def url_for(path)
      self.class.url_for(path)
    end

    def post(path, options={}, &block)
      self.class.post path, options, &block
    end

    def put(path, options={}, &block)
      self.class.put path, options, &block
    end

    def get(path, options={}, &block)
      self.class.get path, options, &block
    end

    def response
      self.class.instance_eval { @response }
    end

    private

    def process_http_request(method, path, options={})
      headers = {
          'Content-Type' => 'application/json',
          'Accept' => 'application/json'
      }
      inject_headers!(headers, options)
      url = url_for path

      log_request method, url, headers, options
      @response = response = send method, url, headers: headers, query: options[:query], body: options[:body]
      log_response @response
      self.class.instance_eval { @response = response }
      @response
    rescue => e
      ApiConnector.logger.error "An exception has occurred while processing request: #{e.inspect}\n#{e.backtrace.join("\n")}"
      raise e
    end

    def log_request(method, url, headers, options)
      ApiConnector.logger.info "#{method.to_s.upcase}: #{url}"
      ApiConnector.logger.debug "Request Headers: #{headers}"
      ApiConnector.logger.debug "Request Query: #{options[:query]}" if options.has_key? :query
      ApiConnector.logger.debug "Request Body: #{options[:body]}" if options.has_key? :body
    end

    def log_response(response)
      ApiConnector.logger.info "GL responded with #{response.code}"
      ApiConnector.logger.debug "Response Body:\n#{response.parsed_response.to_json}"
    end
  end

  module ClassMethods
    def set_settings_from(hash)
      @settings = hash
    end

    def settings
      @settings[target_api]
    end

    def url_for(path)
      path = path[1..-1] if path[0, 1] == '/'
      protocol = settings[:protocol] + ':/'
      address = settings[:address]
      version = @settings[:version]
      [protocol, address, version, path].compact.join '/'
    end
  end

  class << self
    attr_accessor :logger
  end
end
