class ApiSpecsApp
  def self.env
    ENV['RACK_ENV'] ||= 'test'
  end

  def self.logger
    @logger ||= Logger.new "log/#{env}.log", 'daily'
  end

  def self.config=(hash)
    @config = hash
  end

  def self.config
    result = @config[env.to_sym]
    raise "No '#{env}' environment set in config/settings.rb file" unless result
    result
  end
end

require_relative '../config/settings'
