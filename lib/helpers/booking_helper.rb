module BookingHelper
  def job_id
    response.parsed_response['booking']['reservations'].first['job_id']
  end
end
