module Geocoder
  def self.search_string_for(name)
    case name.to_sym
      when :office then '134 w 37 st, NY, NY, US'
      else name
    end
  end

  def geocode(query)
    process_http_request(:get, '/locations', query: {query: query}).parsed_response
  end
end
