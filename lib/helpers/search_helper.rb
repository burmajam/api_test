module SearchHelper
  def limo_result_for_car_class(car_class)
    response.parsed_response["limo_results"].each do |limo_result|
      return limo_result["id"].to_i if limo_result["vehicle_type"] == car_class
    end
    nil
  end
end